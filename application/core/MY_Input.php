<?php
class MY_Input extends CI_Input{
    function __construct(){
        parent::__construct();
    }

    function json($index = null){
        $post = json_decode($this->raw_input_stream, true);

        if($index)
            return isset($post[$index]) ? $post[$index] : null;

        return $post;
    }
}