<?php
class MY_Output extends CI_Output{

    function __construct(){
        parent::__construct();
    }

    function send($output, $status = 200, $type = 'json', $exit = true){
        $this->set_status_header($status);

        if($type === 'json'){
            header('Content-Type: application/json');
            $output = json_encode($output);
        }

        if($exit)
            exit($output);
        else
            echo $output;
    }

}