<?php
class Api extends MX_Controller{

    private $_headers = [
        "Accept: application/vnd.twitchtv.v5+json",
        "Client-ID: uo6dggojyb8d6soh92zknwmi5ej1q2"
    ];

    private $_link = 'https://api.twitch.tv/kraken/streams';

    function __construct(){
        parent:: __construct();
        $this->load->module('auth');

        $data = $this->auth->_checkToken();

        if ( ! $data) {
            $this->output->send(['error' => 'Authorization failed'], 401);
        }
    }

    function streams($channel_id = null){

        $params = $this->input->get();

        if($channel_id){
            $stream = $this->_getById($channel_id, $params);
            if($stream)
                $this->output->send($stream, 200);
            else{
                $this->output->send('Stream not found', 404);
            }
        }else{
            $list = $this->_getList($params);
            $this->output->send($list, 200);
        }
    }

    function _getList(array $params = []){

        $params = $this->_filterParams([
            'stream_type',
            'language',
            'channel',
            'offset',
            'limit',
            'game'
        ], $params);

        if( ! empty($params)){
            $this->_link .= '?' . http_build_query($params);
        }

        $response = $this->_curlRequest($this->_link, $this->_headers);

        if($response['_total'] > 0){
            $list = $response['streams'];
        }else{
            $list = [];
        }

        $streams = [
            'total' => $response['_total'],
            'list' => []
        ];

        foreach($list as $item){
            $streams['list'][] = [
                'stream_id'     => $item['_id'],
                'views'         => $item['viewers'],
                'channel_name'  => $item['channel']['name'],
                'channel_id'    => $item['channel']['_id'],
                'link'          => $item['channel']['url']
            ];
        }

        return $streams;
    }

    function _getById($channel_id, $params){
        if( ! empty($channel_id)){
            $this->_link .= '/' . $channel_id;
        }

        $params = $this->_filterParams([
            'stream_type',
        ], $params);

        if( ! empty($params)){
            $this->_link .= '?' . http_build_query($params);
        }

        $response = $this->_curlRequest($this->_link, $this->_headers);

        if(isset($response['error'])){
            $this->output->send($response, $response['status']);
        }

        if( empty($response['stream'])){
            $this->output->send([
                'error' => 'Not found',
                'status' => 404,
                'message' => 'Stream not found'
            ], 404);
        }

        if($response['stream']){
            return [
                'game'          => $response['stream']['game'],
                'views'         => $response['stream']['viewers'],
                'preview'       => $response['stream']['preview']['medium'],
                'fps'           => $response['stream']['average_fps'],
                'stream_type'   => $response['stream']['stream_type'],
                'created_at'    => $response['stream']['created_at'],
                'channel_name'  => $response['stream']['channel']['name'],
                'link'          => $response['stream']['channel']['url'],
                'logo'          => $response['stream']['channel']['logo']
            ];
        }else{
            return null;
        }
    }

    private function _curlRequest($link, $headers){
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }

    private function _filterParams($allowed_params, $params){
        if(is_array($params)){
            foreach($params as $key => $param){
                if( ! in_array($key, $allowed_params)){
                    unset($params[$key]);
                }
            }
        }
        return $params;
    }
}