<?php
class Migration extends MX_Controller{

    function __construct(){
        parent::__construct();
        $this->load->library('migration');
    }

    function index(){
        $this->migration->version(0);
        $this->migration->latest();
    }

}