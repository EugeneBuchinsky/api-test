<?php
class Main extends MX_Controller{

    public $last_link;

    function __construct(){
        parent::__construct();
        $this->load->module('api');
        $this->last_link = $this->session->userdata('last_link');
        $this->session->set_userdata('last_link', http_build_query($this->input->get()));
    }

    function index($channel_id = null){
        $params = $this->input->get();

        if($channel_id){
            $this->_getById($channel_id, $params);
        }else{
            $this->_getList($params);
        }
    }

    function _getList($params){

        if( ! isset($params['limit'])){
            $params['limit'] = 10;
        }

        if( ! in_array($params['limit'], range(5, 100, 5))){
            $params['limit'] = 10;
        }

        if( ! isset($params['offset'])){
            $params['offset'] = 0;
        }

        $data = [];

        $response = $this->api->_getList($params);

        $data['streams'] = $response['list'];
        $data['total'] = $response['total'];

        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'main/index?limit=' . $params['limit'];
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'offset';
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $params['limit'];

        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();

        $this->load->view('template_tpl', [
            'title' => 'Список стримов',
            'content' => $this->load->view('list_tpl', $data, true)
        ]);
    }

    function _getById($channel_id, $params){
        $data = [];

        $data['stream'] = $this->api->_getById($channel_id, $params);

        $this->load->view('template_tpl', [
            'title' => 'Детали стрима',
            'content' => $this->load->view('details_tpl', $data, true)
        ]);
    }
}