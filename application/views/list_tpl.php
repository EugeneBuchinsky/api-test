<style>
    a:visited, a:link, a:hover{
        color: blue;
    }
    .pagination *{
        display: inline-block;
        padding: 10px 20px;
        margin: 0 5px;
        border: 1px solid silver;
        text-align: center;
        text-decoration: none;
        font-size: 16px;
    }
    .pagination strong{
        background: gray;
        color: white;
    }
    table{
        width: 100%;
        margin: 20px 0;

    }
    table thead td{
        font-size: 20px;
        font-weight: bold;
        padding: 10px 0;
    }
    table td{
        padding: 0 10px;
    }
</style>
<div class="pagination"><?php echo $pagination?></div>
<?php if( ! empty($streams)) {?>
    <table>
        <thead>
            <tr>
                <td width="15%">ID</td>
                <td width="15%">Количество просмотров</td>
                <td width="20%">Название канала</td>
                <td width="15%">Ссылка</td>
                <td width="25%">Информация</td>
            </tr>
        </thead>
        <tbody>
        <?php foreach($streams as $stream) {?>
            <tr>
                <td><?php echo $stream['stream_id']?></td>
                <td><?php echo $stream['views']?></td>
                <td><?php echo $stream['channel_name']?></td>
                <td><a href="<?php echo $stream['link']?>" target="_blank">ссылка на стрим</a></td>
                <td><a href="<?php echo base_url() . 'main/index/' . $stream['channel_id']?>">Детальнее</a></td>
            </tr>
        <?php }?>
        </tbody>
    </table>

<?php }?>
<div class="pagination"><?php echo $pagination?></div>


