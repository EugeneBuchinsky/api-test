<style>
    a:visited, a:link, a:hover{
        color: blue;
    }
    table{
        width: 100%;
        margin: 20px 0;

    }
    table thead td{
        font-size: 20px;
        font-weight: bold;
        padding: 10px 0;
    }
    table td{
        padding: 0 10px;
    }
    .online, .offline{
        display: inline-block;
        width: 20px;
        height: 20px;
        border-radius:20px ;
    }
    .online{
        background: rgb(185, 246, 40);
    }
    .offline{
        background: silver;
    }
</style>
<a href="<?php echo base_url($this->last_link ? '?' . $this->last_link : '')?>">Назад</a>
<table>
    <thead>
    <tr>
        <td width="3%">Статус</td>
        <td width="10%">Превью</td>
        <td width="10%">Название канала</td>
        <td width="10%">FPS</td>
        <td width="10%">Просмотров</td>
        <td width="10%">Ссылка</td>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td><div class="<?php echo $stream['stream_type'] === 'live' ? 'online' : 'offline';?>"></div></td>
            <td><img src="<?php echo $stream['preview']?>" width="200px"></td>
            <td><?php echo $stream['channel_name']?></td>
            <td><?php echo round($stream['fps'])?></td>
            <td><?php echo $stream['views']?></td>
            <td><a href="<?php echo $stream['link']?>" target="_blank">ссылка на стрим</a></td>
        </tr>
</table>
