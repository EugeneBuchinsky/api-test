<?php
class Migration_Create_Users extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->drop_table('users', true);
        $this->dbforge->create_table('users');

        $this->db->insert('users', [
            'email' => 'admin@gmail.com',
            'password' => md5('qwerty'),
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
    }

    function down(){
        $this->dbforge->drop_table('users', true);
    }
}