<?php
class User_model extends CI_Model{

    function getById($id){
        $this->db->where('id', $id);
        $query = $this->db->get('users');
        return $query->row_array();
    }

    function getByEmail($email){
        $this->db->where('email', $email);
        $query = $this->db->get('users');
        return $query->row_array();
    }
}